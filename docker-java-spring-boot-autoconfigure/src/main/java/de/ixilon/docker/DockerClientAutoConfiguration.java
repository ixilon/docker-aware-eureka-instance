package de.ixilon.docker;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;

@Configuration
@ConditionalOnClass(DockerClientBuilder.class)
public class DockerClientAutoConfiguration {

  @Bean
  public DockerClient dockerClient(DockerClientConfig config) {
    return DockerClientBuilder.getInstance(config).build();
  }
  
  @Bean
  @ConditionalOnMissingBean
  public DockerClientConfig dockerClientConfig() {
    return DefaultDockerClientConfig.createDefaultConfigBuilder().build();
  }
  
}
