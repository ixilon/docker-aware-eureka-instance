package de.ixilon.starter.docker;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.context.annotation.Configuration;

import com.github.dockerjava.api.model.Ports.Binding;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.core.DockerClientBuilder;

@Configuration
@ConditionalOnBean({EurekaInstanceConfigBean.class, DockerClient.class})
public class DockerAwareEurekaInstanceAutoConfiguration {

  private static final Log log = LogFactory.getLog(DockerAwareEurekaInstanceAutoConfiguration.class);
  
  public DockerAwareEurekaInstanceAutoConfiguration(EurekaInstanceConfigBean config, DockerClient dockerClient) {
    try {
      for (Binding[] bindings : dockerClient
          .inspectContainerCmd(containerId()).exec()
          .getNetworkSettings()
          .getPorts()
          .getBindings()
          .values()) {
        for (Binding binding : bindings) {
          config.setNonSecurePort(Integer.parseInt(binding.getHostPortSpec()));
        }
      }
    } catch (Exception exception) {
      log.info("unable to determine Docker host port (ignored)", exception);
    }
  }

  private static String containerId() {
    try (
        InputStream inputStream = new FileInputStream("/etc/hostname");
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {
      return bufferedReader.readLine().trim();
    } catch (IOException exception) {
      return System.getenv("HOSTNAME");
    }
  }

}
