[![build status](https://gitlab.com/ixilon/docker-aware-eureka-instance/badges/master/build.svg)](https://gitlab.com/ixilon/docker-aware-eureka-instance/commits/master)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/de.ixilon/docker-aware-eureka-instance-starter/badge.svg)](https://maven-badges.herokuapp.com/maven-central/de.ixilon/docker-aware-eureka-instance-starter)

# docker-aware-eureka-instance #
This is a Spring boot auto-configuration module which registers the mapped host port with Eureka when a webservice is running inside a Docker container.

## Usage ##
Add this JAR to the classpath and mount `/var/run/docker.sock` as volume when starting your Docker container.

```
docker run -P -v /var/run/docker.sock:/var/run/docker.sock <dockerimage>
```

The `-P` option identifies every port with an EXPOSE line in the `Dockerfile` and maps it to a host port somewhere within an ephemeral port range.

This module inspects the created port mapping and registers the host port with Eureka.
